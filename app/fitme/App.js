import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import {f, auth} from './config/config.js';
import SigninScreen from './src/screens/SigninScreen';
import SignupScreen from './src/screens/SignupScreen';
import { createAppContainer, createSwitchNavigator } from 'react-navigation';
import { createStackNavigator} from 'react-navigation-stack';
import { createBottomTabNavigator } from 'react-navigation-tabs';
import LoadingScreen from './src/screens/LoadingScreen';
import Dashboard from './src/screens/DashboardScreen';
import Workout from './src/screens/WorkoutScreen';
import Progress from './src/screens/ProgressScreen';
import Activity from './src/screens/ActivityScreen';
import Gyms from './src/screens/GymsScreen';
import PlanWorkout from './src/screens/PlanWorkoutScreen';
import WorkoutList from './src/screens/WorkoutListScreen';
import { Provider as AuthProvider } from './src/context/AuthContext';

const switchNavigator = createSwitchNavigator({
  loginFlow: createStackNavigator({
    Signin: SigninScreen,
    Signup: SignupScreen
   
  }),
  mainFlow: createStackNavigator({
    Dashboard: Dashboard,
    Workout: Workout,
    Progress: Progress,
    Activity: Activity,
    Gyms: Gyms,
    PlanWorkout: PlanWorkout,
    WorkoutList: WorkoutList
  })

});

export default createAppContainer(switchNavigator);
