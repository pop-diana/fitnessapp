import firebase from 'firebase'

//api details
const firebaseConfig = {
    apiKey: "AIzaSyB7-Yz9Y9BvTQ4xK6121n3NZ-zynhT4lyc",
    authDomain: "fitme-mobile-app.firebaseapp.com",
    databaseURL: "https://fitme-mobile-app.firebaseio.com/",
    projectId: "fitme-mobile-ap",
    //storageBucket: "****",
    //messagingSenderId: "****",
    //appId: "****",
    //measurementId: "****"
  };

firebase.initializeApp(firebaseConfig)

export const f = firebase;
export const database = firebase.database();
export const auth = firebase.auth();
export const storage = firebase.storage();