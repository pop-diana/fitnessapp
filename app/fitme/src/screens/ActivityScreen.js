import React, { Component } from 'react';
import { Pedometer } from 'expo-sensors';
import {
  Text,
  TextInput,
  StyleSheet,
  View,
  SafeAreaView,
  TouchableOpacity,
  ActivityIndicator,
} from 'react-native';

import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import { LinearGradient } from 'expo-linear-gradient';

const COLORS = {
  WHITE: '#FFF',
  BLACK: '#000',
  BLUE: '#69B1D6',
  ORANGE: '#F7773B',
  DARK_ORANGE: '#FE5000',
  GREY: '#AFAFAF',
  DARK_GREY: '#90919E',
};

const SIZES = {
  BASE: 6,
  FONT: 14,
  TITLE: 34,
  SUBTITLE: 20,
  LABEL: 12,
  PADDING: 9
};

export default class Activity extends Component {


  static navigationOptions = {
    headerShown: false
  }

  constructor(props) {
    super(props);
    this.state = {
      loggedin: true,
      loading: false,
      isPedometerAvailable: 'checking',
      pastStepCount: 0,
      currentStepCount: 0,
    };
    var that = this;
  }

  componentDidMount() {
    this._subscribe();
  }

  componentWillUnmount() {
    this._unsubscribe();
  }

  _subscribe = () => {
    this._subscription = Pedometer.watchStepCount(result => {
      this.setState({
        currentStepCount: result.steps,
      });
    });

    Pedometer.isAvailableAsync().then(
      result => {
        this.setState({
          isPedometerAvailable: String(result),
        });
      },
      error => {
        this.setState({
          isPedometerAvailable: 'Could not get isPedometerAvailable: ' + error,
        });
      }
    );
    const end = new Date();
    const start = new Date();
    start.setDate(end.getDate() - 1);
    Pedometer.getStepCountAsync(start, end).then(
      result => {
        this.setState({ pastStepCount: result.steps });
      },
      error => {
        this.setState({
          pastStepCount: 'Could not get stepCount: ' + error,
        });
      }
    );
  };

  _unsubscribe = () => {
    this._subscription && this._subscription.remove();
    this._subscription = null;
  };

  openSignup = () => {
    this.props.navigation.navigate('Signup');
  };


  render() {
    const { loggedin, loading } = this.state;
    return (
      <>

        <View >
          <Text style={styles.title}>Your Activity</Text>
          <Text style={styles.subtitle}>Steps taken today: {this.state.currentStepCount}</Text>
          <Icon
            name='walk'
            size={75}
            style={{ alignSelf: 'center', marginTop: '5%' }}
            color='#FE5000' />
          <Text style={{
            color: COLORS.BLACK,
            fontSize: SIZES.SUBTITLE,
            alignSelf: 'center',
            marginTop: '5%',
          }}>Keep walking!</Text>
        </View>

      </>
    )
  }
}

const styles = StyleSheet.create({
  button: {
    alignItems: 'center',
    borderRadius: SIZES.BASE,
    justifyContent: 'center',
    padding: SIZES.PADDING / 0.83,
  },
  container: {
    flex: 1,
    paddingHorizontal: SIZES.PADDING,
    paddingVertical: SIZES.PADDING * 2,
  },
  divider: {
    alignItems: 'center',
    backgroundColor: COLORS.GREY,
    height: StyleSheet.hairlineWidth,
    justifyContent: 'center',
    marginVertical: SIZES.PADDING * 2,
    width: '50%',
  },
  dividerLabel: {
    backgroundColor: COLORS.WHITE,
    color: COLORS.GREY,
    fontSize: SIZES.SUBTITLE,
    paddingHorizontal: SIZES.BASE,
    position: 'absolute',
  },
  input: {
    borderColor: COLORS.GREY,
    borderRadius: SIZES.BASE,
    borderWidth: StyleSheet.hairlineWidth,
    fontSize: SIZES.FONT,
    padding: SIZES.PADDING * 1.5,
  },
  inputContainer: {
    marginBottom: SIZES.PADDING,
    paddingLeft: 30,
    paddingRight: 30
  },
  label: {
    color: COLORS.DARK_GREY,
    fontSize: SIZES.FONT,
    marginBottom: SIZES.BASE,
  },
  signin: {
    paddingVertical: SIZES.PADDING * 1.33,
  },
  social: {
    flex: 0,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  socialLabel: {
    color: COLORS.WHITE,
    flex: 1,
    fontWeight: '500',
    textAlign: 'center',
  },
  subtitle: {
    color: COLORS.ORANGE,
    fontSize: SIZES.SUBTITLE,
    alignSelf: 'center',
    marginTop: '5%',
  },
  title: {
    fontSize: SIZES.TITLE,
    fontWeight: '600',
    letterSpacing: 1,
    marginBottom: SIZES.BASE,
    color: COLORS.BLACK,
    marginTop: '50%',
    alignSelf: 'center'
  },
});