import React from 'react';
import { Text, StyleSheet, Button, View } from 'react-native';

const LoadingScreen = ({navigation}) => {

  return(
    <View style={styles.container}>
        <Text>Loading</Text>
        <ActivityIndicator size="large" />
      </View>
  );
};

const styles = StyleSheet.create({
    container: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
    }
  })

export default LoadingScreen;
