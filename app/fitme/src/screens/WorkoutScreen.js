import React, { Component } from 'react';
import {
  Text,
  TextInput,
  StyleSheet,
  View,
  SafeAreaView,
  TouchableOpacity,
  ActivityIndicator,
} from 'react-native';

import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import { LinearGradient } from 'expo-linear-gradient';

const COLORS = {
  WHITE: '#FFF',
  BLACK: '#000',
  BLUE: '#69B1D6',
  ORANGE: '#F7773B',
  DARK_ORANGE: '#FE5000',
  GREY: '#AFAFAF',
  DARK_GREY: '#90919E',
};

const SIZES = {
  BASE: 6,
  FONT: 14,
  TITLE: 34,
  SUBTITLE: 11,
  LABEL: 12,
  PADDING: 9
};

export default class Workout extends Component {


  static navigationOptions = {
    headerShown: false
  }

  constructor(props) {
    super(props);
    this.state = {
      loggedin: true,
      loading: false,
      user: props.navigation.state.params.user,
    };
    var that = this;
  }

  openPlanWorkout = (user) => {
    this.props.navigation.navigate('PlanWorkout', {user});
  };

  openWorkoutList = (user) => {
    this.props.navigation.navigate('WorkoutList', {user});
  };


  render() {
    const { loggedin, loading, user } = this.state;
    return (
      <>

        <View >
        <Text style={styles.title}>Workouts</Text>
          <TouchableOpacity
            style={{ paddingTop: '10%', paddingRight: '10%', paddingLeft: '10%' }}
            onPress={() => this.openPlanWorkout(user)}>
            <LinearGradient
              style={[styles.button, styles.signin]}
              colors={[COLORS.ORANGE, COLORS.DARK_ORANGE]}
              start={{ x: 0, y: 1 }}
              end={{ x: 1, y: 0 }}>
              {loading ? (
                <ActivityIndicator size={SIZES.FONT * 1.4} color={COLORS.WHITE} />
              ) : (
                  <View>
                    <Icon
                      name='calendar'
                      size={20}
                      color='#ffffff'
                      style={{ alignSelf: 'center' }} />
                    <Text
                      style={{
                        fontWeight: '500',
                        letterSpacing: 0.5,
                        color: COLORS.WHITE,
                        backgroundColor: 'transparent',
                      }}>

                      Plan workout
                  </Text>
                  </View>
                )}
            </LinearGradient>
          </TouchableOpacity>
          <TouchableOpacity
            style={{ paddingTop: '10%', paddingRight: '10%', paddingLeft: '10%' }}
            onPress={() => this.openWorkoutList(user)}>
            <LinearGradient
              style={[styles.button, styles.signin]}
              colors={[COLORS.ORANGE, COLORS.DARK_ORANGE]}
              start={{ x: 0, y: 1 }}
              end={{ x: 1, y: 0 }}>
              {loading ? (
                <ActivityIndicator size={SIZES.FONT * 1.4} color={COLORS.WHITE} />
              ) : (
                  <View>
                    <Icon
                      name='clipboard-text'
                      size={20}
                      color='#ffffff'
                      style={{ alignSelf: 'center' }}
                    />
                    <Text
                      style={{
                        fontWeight: '500',
                        letterSpacing: 0.5,
                        color: COLORS.WHITE,
                        backgroundColor: 'transparent',
                      }}>

                      Workout list
                                    </Text>

                  </View>
                )}

            </LinearGradient>
          </TouchableOpacity>
        </View>

      </>
    )
  }
}

const styles = StyleSheet.create({
  button: {
    alignItems: 'center',
    borderRadius: SIZES.BASE,
    justifyContent: 'center',
    padding: SIZES.PADDING / 0.83,
  },
  container: {
    flex: 1,
    paddingHorizontal: SIZES.PADDING,
    paddingVertical: SIZES.PADDING * 2,
  },
  divider: {
    alignItems: 'center',
    backgroundColor: COLORS.GREY,
    height: StyleSheet.hairlineWidth,
    justifyContent: 'center',
    marginVertical: SIZES.PADDING * 2,
    width: '50%',
  },
  dividerLabel: {
    backgroundColor: COLORS.WHITE,
    color: COLORS.GREY,
    fontSize: SIZES.SUBTITLE,
    paddingHorizontal: SIZES.BASE,
    position: 'absolute',
  },
  input: {
    borderColor: COLORS.GREY,
    borderRadius: SIZES.BASE,
    borderWidth: StyleSheet.hairlineWidth,
    fontSize: SIZES.FONT,
    padding: SIZES.PADDING * 1.5,
  },
  inputContainer: {
    marginBottom: SIZES.PADDING,
    paddingLeft: 30,
    paddingRight: 30
  },
  label: {
    color: COLORS.DARK_GREY,
    fontSize: SIZES.FONT,
    marginBottom: SIZES.BASE,
  },
  signin: {
    paddingVertical: SIZES.PADDING * 1.33,
  },
  social: {
    flex: 0,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  socialLabel: {
    color: COLORS.WHITE,
    flex: 1,
    fontWeight: '500',
    textAlign: 'center',
  },
  subtitle: {
    color: COLORS.GREY,
    fontSize: SIZES.SUBTITLE,
  },
  title: {
    fontSize: SIZES.TITLE,
    fontWeight: '600',
    letterSpacing: 1,
    marginBottom: SIZES.BASE,
    color: COLORS.DARK_ORANGE,
    marginTop: '30%',
    alignSelf: 'center'
  },
});