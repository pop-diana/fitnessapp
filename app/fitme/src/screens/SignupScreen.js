import React, { Component } from 'react';
import {
  Alert,
  Text,
  TextInput,
  StyleSheet,
  View,
  SafeAreaView,
  TouchableOpacity,
  ActivityIndicator,
} from 'react-native';

import { LinearGradient } from 'expo-linear-gradient';
import { FontAwesome } from '@expo/vector-icons';
import { f, auth, database } from '../../config/config';

const COLORS = {
  WHITE: '#FFF',
  BLACK: '#000',
  BLUE: '#69B1D6',
  ORANGE: '#F7773B',
  DARK_ORANGE: '#FE5000',
  GREY: '#AFAFAF',
  DARK_GREY: '#90919E',
  FACEBOOK: '#3A5896',
};

const SIZES = {
  BASE: 6,
  FONT: 14,
  TITLE: 26,
  SUBTITLE: 11,
  LABEL: 12,
  PADDING: 9
};



export default class SignupScreen extends Component {
  static navigationOptions = {
    headerShown: false,
  };
  constructor(props) {
    super(props);
    this.state = {
      loggedin: false,
      loading: false,
      email: null,
      password: null,
      confirmPass: null,
    };

    var that = this;
   
    f.auth().onAuthStateChanged(function (user) {

      if (user) {
        that.setState({
          loggedin: true
        });
      } else {
        that.setState({
          loggedin: false
        });
      }


    });

  }

  openSignin = () => {
    this.props.navigation.navigate('Signin');
  };

  openDashboard = () => {
    this.props.navigation.navigate('Dashboard');
  }

  registerUser = (email, password, confirmPass) => {

    if (password == confirmPass) {
      auth.createUserWithEmailAndPassword(email, password)
        .then((user) => {
          this.openDashboard();
        }).catch((error) => alert(error));
    } else {
      alert(`Passwords do not match`);
    }



  }

  signUserOut = () => {
    auth.signOut()
      .then(() => {
      }).catch((error) => {
        console.log('error', error)
      });
  }

  renderInputs() {
    const { email, password, loading, confirmPass } = this.state;
    const isValid = email && password;

    return (
      <>
        <View style={styles.inputContainer}>

          <TextInput
            value={email}
            style={styles.input}
            placeholder="Email address"
            placeholderTextColor={COLORS.DARK_GREY}
            onChangeText={value => this.setState({ email: value })}
          />
        </View>
        <View style={styles.inputContainer}>

          <TextInput
            secureTextEntry
            value={password}
            placeholder="Password"
            style={styles.input}
            placeholderTextColor={COLORS.DARK_GREY}
            onChangeText={value => this.setState({ password: value })}
          />
        </View>
        <View style={styles.inputContainer}>

          <TextInput
            secureTextEntry
            value={confirmPass}
            placeholder="Confirm password"
            style={styles.input}
            placeholderTextColor={COLORS.DARK_GREY}
            onChangeText={value => this.setState({ confirmPass: value })}
          />
        </View>
        <TouchableOpacity
          disabled={!isValid}
          style={{ marginTop: SIZES.PADDING * 1.5, paddingLeft: 30, paddingRight: 30 }}
          onPress={() => this.registerUser(this.state.email, this.state.password, this.state.confirmPass)}>
          <LinearGradient
            style={[styles.button, styles.signin]}
            colors={isValid ? [COLORS.ORANGE, COLORS.DARK_ORANGE] : [COLORS.GREY, COLORS.DARK_GREY]}
            start={{ x: 0, y: 1 }}
            end={{ x: 1, y: 0 }}>
            {loading ? (
              <ActivityIndicator size={SIZES.FONT * 1.4} color={COLORS.WHITE} />
            ) : (
                <Text
                  style={{
                    fontWeight: '500',
                    letterSpacing: 0.5,
                    color: COLORS.WHITE,
                    backgroundColor: 'transparent',
                  }}>
                  Submit
                </Text>
              )}
          </LinearGradient>
        </TouchableOpacity>
      </>
    );
  }



  render() {
    return (
      <SafeAreaView style={{ flex: 1 }}>
        <View style={styles.container}>
          <View style={{ marginBottom: 30, alignItems: "center", marginTop: 120 }}>
            <Text style={styles.title}>Register as a new user</Text>
          </View>
          <View style={{ flex: 2 }}>
            {this.renderInputs()}

          </View>
          <View style={{ flex: 0.50, alignItems: 'center' }}>
            <Text
              style={{
                fontSize: SIZES.FONT,
                color: COLORS.GREY,
                marginBottom: SIZES.BASE,
              }}>
              Already have an account?
            </Text>
            <TouchableOpacity onPress={() => this.openSignin()}>
              <Text
                style={{
                  fontSize: SIZES.FONT,
                  fontWeight: '600',
                  color: COLORS.ORANGE,
                }}>
                Sign in
              </Text>
            </TouchableOpacity>
          </View>
        </View>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  button: {
    alignItems: 'center',
    borderRadius: SIZES.BASE,
    justifyContent: 'center',
    padding: SIZES.PADDING / 0.83,
  },
  container: {
    flex: 1,
    paddingHorizontal: SIZES.PADDING,
    paddingVertical: SIZES.PADDING * 2,
  },
  divider: {
    alignItems: 'center',
    backgroundColor: COLORS.GREY,
    height: StyleSheet.hairlineWidth,
    justifyContent: 'center',
    marginVertical: SIZES.PADDING * 2,
    width: '50%',
  },
  dividerLabel: {
    backgroundColor: COLORS.WHITE,
    color: COLORS.GREY,
    fontSize: SIZES.SUBTITLE,
    paddingHorizontal: SIZES.BASE,
    position: 'absolute',
  },
  input: {
    borderColor: COLORS.GREY,
    borderRadius: SIZES.BASE,
    borderWidth: StyleSheet.hairlineWidth,
    fontSize: SIZES.FONT,
    padding: SIZES.PADDING * 1.5,
  },
  inputContainer: {
    marginBottom: SIZES.PADDING,
    paddingLeft: 30,
    paddingRight: 30
  },
  label: {
    color: COLORS.DARK_GREY,
    fontSize: SIZES.FONT,
    marginBottom: SIZES.BASE,
  },
  signin: {
    paddingVertical: SIZES.PADDING * 1.33,
  },
  social: {
    flex: 0,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  socialLabel: {
    color: COLORS.WHITE,
    flex: 1,
    fontWeight: '500',
    textAlign: 'center',
  },
  subtitle: {
    color: COLORS.GREY,
    fontSize: SIZES.SUBTITLE,
  },
  title: {
    fontSize: SIZES.TITLE,
    fontWeight: '600',
    letterSpacing: 1,
    marginBottom: SIZES.BASE,
    color: COLORS.ORANGE
  },
});
