import React, { Component } from 'react';

import {
    View,
    StyleSheet,
    Dimensions,
    Alert
} from 'react-native';

import MapView from 'react-native-maps';
import NetInfo from "@react-native-community/netinfo";

const { width, height } = Dimensions.get('window')

const SCREEN_HEIGHT = height
const SCREEN_WIDTH = width
const ASPECT_RATIO = width / height
const LATITUDE_DELTA = 0.0922
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO

const COLORS = {
    WHITE: '#FFF',
    BLACK: '#000',
    BLUE: '#69B1D6',
    ORANGE: '#F7773B',
    DARK_ORANGE: '#FE5000',
    GREY: '#AFAFAF',
    DARK_GREY: '#90919E',
};

const SIZES = {
    BASE: 6,
    FONT: 14,
    TITLE: 28,
    SUBTITLE: 11,
    LABEL: 12,
    PADDING: 9
};

const markers = [
    {
        coordinate: {
            latitude: 44.311938,
            longitude: 23.7915323
        },
        title: 'Star Gym',
        description: '',
        id: 1
    },
    {
        coordinate: {
            latitude: 44.3013471,
            longitude: 23.7930769
        },
        title: 'Kiry Muscle Gym',
        description: '',
        id: 2
    },
    {
        coordinate: {
            latitude: 44.3162463,
            longitude: 23.8349656
        },
        title: '4Moving Metal Lemn',
        description: '',
        id: 3
    },
    {
        coordinate: {
            latitude: 44.3203724,
            longitude: 23.8185271
        },
        title: 'Activ Gym',
        description: '',
        id: 4
    },
    {
        coordinate: {
            latitude: 44.3195092,
            longitude: 23.8073044
        },
        title: '4Moving Piata Centrala',
        description: '',
        id: 5
    }
];

class Gyms extends Component {
    constructor() {
        super()
        this.state = {
            initialPosition: {
                latitude: 0,
                longitude: 0,
                latitudeDelta: 0,
                longitudeDelta: 0,
            },
        }
        const connectionSubscription = NetInfo.addEventListener(state => {
            if (!state.isConnected) {
                Alert.alert("You are offline! Please check your connection.");
            }
        });


    }

    componentDidMount() {
        navigator.geolocation.getCurrentPosition((position) => {
            var lat = parseFloat(position.coords.latitude)
            var long = parseFloat(position.coords.longitude)

            var initialRegion = {
                latitude: lat,
                longitude: long,
                latitudeDelta: 0.0041,
                longitudeDelta: 0.0021,
            }
            if (this.mapView) {
                this.mapView.animateToRegion(initialRegion)
            }
            this.setState(initialRegion)

          
        },
            (error) => { },
            { enableHighAccuracy: true, timeout: 20000, maximumAge: 1000 });
    }

    setState(initialRegion) {
        this.state.initialPosition = initialRegion;
    }

    renderScreen = () => {
        return (
            <View style={styles.container}>
                <MapView
                    ref={ref => this.mapView = ref}
                    showsUserLocation //to show user current location when given access
                    loadingEnabled //to show loading while map loading
                    followsUserLocation
                    style={styles.map}
                    annotations={[markers]}
                >
                    {markers.map((marker) => (
                        <MapView.Marker
                            key={marker.id}
                            coordinate={marker.coordinate}
                            title={marker.title}
                            description={marker.description}
                        />
                    ))}
                </MapView>

            </View>
        );
    }

    render() {
        return (
            this.renderScreen()
        );
    }
}

const styles = StyleSheet.create({
    container: {
        position: 'absolute',
        top: 0,
        left: 0,
        right: 0,
        bottom: 0,
        justifyContent: 'flex-end',
        alignItems: 'center',
    },
    map: {
        position: 'absolute',
        top: 0,
        left: 0,
        right: 0,
        bottom: 0,
    },
    title: {
        fontSize: SIZES.TITLE,
        fontWeight: '600',
        letterSpacing: 1,
        color: COLORS.ORANGE,
        marginBottom: '130%',
        marginLeft: 20,
        marginRight: 20,
        backgroundColor: COLORS.WHITE,
    }
});

export default Gyms;