import React, { Component, ReactDOM } from 'react';
import {
  Text,
  TextInput,
  StyleSheet,
  View,
  ScrollView,
  SafeAreaView,
  TouchableOpacity,
  ActivityIndicator,
  Image
} from 'react-native';
import { Camera } from 'expo-camera';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import { LinearGradient } from 'expo-linear-gradient';
import GridList from 'react-native-grid-list';
import * as MediaLibrary from 'expo-media-library';

const COLORS = {
  WHITE: '#FFF',
  BLACK: '#000',
  BLUE: '#69B1D6',
  ORANGE: '#F7773B',
  DARK_ORANGE: '#FE5000',
  GREY: '#AFAFAF',
  DARK_GREY: '#90919E',
};

const SIZES = {
  BASE: 6,
  FONT: 14,
  TITLE: 34,
  SUBTITLE: 11,
  LABEL: 12,
  PADDING: 9
};


export default class Progress extends Component {


  static navigationOptions = {
    headerShown: false
  }

  renderImage = ({ item }) => (
    <Image style={styles.image} source={item} />
  );

  constructor(props) {
    super(props);
    this.state = {
      loggedin: true,
      loading: false,
      user: props.navigation.state.params.user,
      cameraMode: false,
      type: Camera.Constants.Type.back,
      photos: new Array(),
    };
    var that = this;  
  }

  setCameraMode() {
    this.setState({ cameraMode: !this.state.cameraMode });
  }

  snap = async () => {
    if (this.camera) {
      let photo = await this.camera.takePictureAsync();
      let asset = await MediaLibrary.createAssetAsync(photo.uri);
      let newPhotos = this.state.photos;
      newPhotos.push(photo);
      this.setState({photos: newPhotos, cameraMode: !this.state.cameraMode});
    }
  };

  render() {
    const { cameraMode, type, photos} = this.state;
    if (cameraMode == true) {
      return (
        <View style={{ flex: 1 }}>
          <Camera ref={ref => {
            this.camera = ref;
          }} style={{ flex: 1 }} type={type}>
            <View
              style={{
                flex: 1,
                backgroundColor: 'transparent',
                flexDirection: 'row',
              }}>
              <TouchableOpacity
                style={{
                  flex: 1,
                  alignSelf: 'flex-end',
                  alignItems: 'center',
                }}
                onPress={() => {
                  this.snap()
                }}>
                <View>
                  <Icon
                    name='camera'
                    marginBottom={100}
                    color={COLORS.WHITE}
                    size={50} />
                </View>

              </TouchableOpacity>
            </View>
          </Camera>
        </View>
      );
    } else {
      return (
        <>
         <ScrollView style={styles.container} showsVerticalScrollIndicator={false}>
          <View >
            <Text style={styles.title}> Progress</Text>
            <GridList
            showSeparator
            data={photos}
            numColumns={2}
            renderItem={this.renderImage}
            separatorBorderWidth={10}
            separatorBorderColor={'transparent'}
          />
            <TouchableOpacity
              style={{ marginTop: '10%', paddingRight: '35%', paddingLeft: '35%'}}
              onPress={() => this.setCameraMode()}>
              <LinearGradient
                style={[styles.button, styles.signin]}
                colors={[COLORS.ORANGE, COLORS.DARK_ORANGE]}
                start={{ x: 0, y: 1 }}
                end={{ x: 1, y: 0 }}>
                <View>
                  <Icon
                    name='camera'
                    size={50}
                    style={{ alignSelf: 'center' }}
                    color='#ffffff' />
                </View>

              </LinearGradient>

            </TouchableOpacity>
          </View>
          </ScrollView>
        </>
      )
    }
  }
}

const styles = StyleSheet.create({
  button: {
    alignItems: 'center',
    borderRadius: SIZES.BASE,
    justifyContent: 'center',
    padding: SIZES.PADDING / 0.83,
  },
  container: {
    flex: 1,
    margin: '5%',
  },
  divider: {
    alignItems: 'center',
    backgroundColor: COLORS.GREY,
    height: StyleSheet.hairlineWidth,
    justifyContent: 'center',
    marginVertical: SIZES.PADDING * 2,
    width: '50%',
  },
  dividerLabel: {
    backgroundColor: COLORS.WHITE,
    color: COLORS.GREY,
    fontSize: SIZES.SUBTITLE,
    paddingHorizontal: SIZES.BASE,
    position: 'absolute',
  },
  input: {
    borderColor: COLORS.GREY,
    borderRadius: SIZES.BASE,
    borderWidth: StyleSheet.hairlineWidth,
    fontSize: SIZES.FONT,
    padding: SIZES.PADDING * 1.5,
  },
  inputContainer: {
    marginBottom: SIZES.PADDING,
    paddingLeft: 30,
    paddingRight: 30
  },
  label: {
    color: COLORS.DARK_GREY,
    fontSize: SIZES.FONT,
    marginBottom: SIZES.BASE,
  },
  signin: {
    paddingVertical: SIZES.PADDING * 1.33,
  },
  social: {
    flex: 0,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  socialLabel: {
    color: COLORS.WHITE,
    flex: 1,
    fontWeight: '500',
    textAlign: 'center',
  },
  subtitle: {
    color: COLORS.GREY,
    fontSize: SIZES.SUBTITLE,
  },
  title: {
    fontSize: SIZES.TITLE,
    fontWeight: '600',
    letterSpacing: 1,
    marginBottom: SIZES.BASE,
    color: COLORS.DARK_ORANGE,
    marginTop: '10%',
    marginBottom: '5%',
    alignSelf: 'center'
  },
  girdSeparator: {
    borderWidth: 1,
  },
  image: {
    width: '100%',
    height: '100%',
    borderRadius: 10,
  },
});

