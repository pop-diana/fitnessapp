import React, { Component } from 'react';
import {
    Text,
    TextInput,
    StyleSheet,
    View,
    SafeAreaView,
    TouchableOpacity,
    ActivityIndicator,
    Alert,
    Button, Vibration, Platform
} from 'react-native';

import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import { LinearGradient } from 'expo-linear-gradient';
import { Notifications } from 'expo';
import * as Permissions from 'expo-permissions';
import Constants from 'expo-constants';



const COLORS = {
    WHITE: '#FFF',
    BLACK: '#000',
    BLUE: '#69B1D6',
    ORANGE: '#F7773B',
    DARK_ORANGE: '#FE5000',
    GREY: '#AFAFAF',
    DARK_GREY: '#90919E',
};

const SIZES = {
    BASE: 6,
    FONT: 14,
    TITLE: 28,
    SUBTITLE: 11,
    LABEL: 12,
    PADDING: 9
};

export default class Dashboard extends Component {



    static navigationOptions = {
        headerShown: false
    }

    constructor(props) {
        super(props);
        this.state = {
            loggedin: true,
            loading: false,
            user: props.navigation.state.params.user,
            expoPushToken: '',
            notification: {}
        };

        var that = this;
    }

    registerForPushNotificationsAsync = async () => {
        if (Constants.isDevice) {
            const { status: existingStatus } = await Permissions.getAsync(Permissions.NOTIFICATIONS);
            let finalStatus = existingStatus;
            if (existingStatus !== 'granted') {
                const { status } = await Permissions.askAsync(Permissions.NOTIFICATIONS);
                finalStatus = status;
            }
            if (finalStatus !== 'granted') {
                alert('Failed to get push token for push notification!');
                return;
            }
            let token = await Notifications.getExpoPushTokenAsync();
            this.setState({ expoPushToken: token });
        } else {
            alert('Must use physical device for Push Notifications');
        }

        if (Platform.OS === 'android') {
            Notifications.createChannelAndroidAsync('default', {
                name: 'default',
                sound: true,
                priority: 'max',
                vibrate: [0, 250, 250, 250],
            });
        }
    };

    componentDidMount() {
        this.registerForPushNotificationsAsync();  
        this._notificationSubscription = Notifications.addListener(this._handleNotification);       
    }

    _handleNotification = notification => {
        Vibration.vibrate();
        this.setState({ notification: notification });
    };
    

    sendPushNotification = async () => {
        const message = {
          to: this.state.expoPushToken,
          sound: 'default',
          title: 'Welcome back!',
          body: 'Ready to be active? Set! Go!',
          data: { data: '' },
          _displayInForeground: true,
        };
        const response = await fetch('https://exp.host/--/api/v2/push/send', {
          method: 'POST',
          headers: {
            Accept: 'application/json',
            'Accept-encoding': 'gzip, deflate',
            'Content-Type': 'application/json',
          },
          body: JSON.stringify(message),
        });
     };

    openWorkout = (user) => {
        this.sendPushNotification();
        this.props.navigation.navigate('Workout', { user });
    };

    openProgress = (user) => {
        
        this.props.navigation.navigate('Progress', { user });
    };

    openActivity = () => {
        this.props.navigation.navigate('Activity');
    };

    openGym = () => {
        this.props.navigation.navigate('Gyms');
    };


    render() {
        const { loggedin, loading, user } = this.state;
        return (
            <>

                <View >
                    <Text style={styles.title}>fitme</Text>
                    <TouchableOpacity
                        style={{ marginTop: '10%', paddingRight: '10%', paddingLeft: '10%' }}
                        onPress={() => this.openWorkout(user)}>
                        <LinearGradient
                            style={[styles.button, styles.signin]}
                            colors={[COLORS.ORANGE, COLORS.DARK_ORANGE]}
                            start={{ x: 0, y: 1 }}
                            end={{ x: 1, y: 0 }}>
                            {loading ? (
                                <ActivityIndicator size={SIZES.FONT * 1.4} color={COLORS.WHITE} />
                            ) : (

                                    <View>
                                        <Icon
                                            name='dumbbell'
                                            size={20}
                                            style={{ alignSelf: 'center' }}
                                            color='#ffffff' />
                                        <Text
                                            style={{
                                                fontWeight: '500',
                                                letterSpacing: 0.5,
                                                color: COLORS.WHITE,
                                                backgroundColor: 'transparent',
                                            }}>

                                            Workout
                                   </Text>
                                    </View>
                                )}
                        </LinearGradient>
                    </TouchableOpacity>
                    <TouchableOpacity
                        style={{ marginTop: '10%', paddingRight: '10%', paddingLeft: '10%' }}
                        onPress={() => this.openProgress(user)}>
                        <LinearGradient
                            style={[styles.button, styles.signin]}
                            colors={[COLORS.ORANGE, COLORS.DARK_ORANGE]}
                            start={{ x: 0, y: 1 }}
                            end={{ x: 1, y: 0 }}>
                            {loading ? (
                                <ActivityIndicator size={SIZES.FONT * 1.4} color={COLORS.WHITE} />
                            ) : (
                                    <View>
                                        <Icon
                                            name='trending-up'
                                            size={20}
                                            style={{ alignSelf: 'center' }}
                                            color='#ffffff' />
                                        <Text
                                            style={{
                                                fontWeight: '500',
                                                letterSpacing: 0.5,
                                                color: COLORS.WHITE,
                                                backgroundColor: 'transparent',
                                            }}>

                                            Progress
                                   </Text>
                                    </View>
                                )}
                        </LinearGradient>
                    </TouchableOpacity>
                    <TouchableOpacity
                        style={{ marginTop: '10%', paddingRight: '10%', paddingLeft: '10%' }}
                        onPress={() => this.openActivity()}>
                        <LinearGradient
                            style={[styles.button, styles.signin]}
                            colors={[COLORS.ORANGE, COLORS.DARK_ORANGE]}
                            start={{ x: 0, y: 1 }}
                            end={{ x: 1, y: 0 }}>
                            {loading ? (
                                <ActivityIndicator size={SIZES.FONT * 1.4} color={COLORS.WHITE} />
                            ) : (
                                    <View>
                                        <Icon
                                            name='walk'
                                            size={20}
                                            style={{ alignSelf: 'center' }}
                                            color='#ffffff' />
                                        <Text
                                            style={{
                                                fontWeight: '500',
                                                letterSpacing: 0.5,
                                                color: COLORS.WHITE,
                                                backgroundColor: 'transparent',
                                            }}>

                                            Activity
                                    </Text>
                                    </View>
                                )}
                        </LinearGradient>
                    </TouchableOpacity>
                    <TouchableOpacity
                        style={{ marginTop: '10%', paddingRight: '10%', paddingLeft: '10%' }}
                        onPress={() => this.openGym()}>
                        <LinearGradient
                            style={[styles.button, styles.signin]}
                            colors={[COLORS.ORANGE, COLORS.DARK_ORANGE]}
                            start={{ x: 0, y: 1 }}
                            end={{ x: 1, y: 0 }}>
                            {loading ? (
                                <ActivityIndicator size={SIZES.FONT * 1.4} color={COLORS.WHITE} />
                            ) : (
                                    <View>
                                        <Icon
                                            name='compass'
                                            size={20}
                                            style={{ alignSelf: 'center' }}
                                            color='#ffffff' />
                                        <Text
                                            style={{
                                                fontWeight: '500',
                                                letterSpacing: 0.5,
                                                color: COLORS.WHITE,
                                                backgroundColor: 'transparent',
                                            }}>

                                            Gym
                                    </Text>
                                    </View>
                                )}
                        </LinearGradient>
                    </TouchableOpacity>

                </View>

            </>
        )
    }
}

const styles = StyleSheet.create({
    button: {
        alignItems: 'center',
        borderRadius: SIZES.BASE,
        justifyContent: 'center',
        padding: SIZES.PADDING / 0.83,
    },
    container: {
        flex: 1,
        paddingHorizontal: SIZES.PADDING,
        paddingVertical: SIZES.PADDING * 2,
    },
    divider: {
        alignItems: 'center',
        backgroundColor: COLORS.GREY,
        height: StyleSheet.hairlineWidth,
        justifyContent: 'center',
        marginVertical: SIZES.PADDING * 2,
        width: '50%',
    },
    dividerLabel: {
        backgroundColor: COLORS.WHITE,
        color: COLORS.GREY,
        fontSize: SIZES.SUBTITLE,
        paddingHorizontal: SIZES.BASE,
        position: 'absolute',
    },
    input: {
        borderColor: COLORS.GREY,
        borderRadius: SIZES.BASE,
        borderWidth: StyleSheet.hairlineWidth,
        fontSize: SIZES.FONT,
        padding: SIZES.PADDING * 1.5,
    },
    inputContainer: {
        marginBottom: SIZES.PADDING,
        paddingLeft: 30,
        paddingRight: 30
    },
    label: {
        color: COLORS.DARK_GREY,
        fontSize: SIZES.FONT,
        marginBottom: SIZES.BASE,
    },
    signin: {
        paddingVertical: SIZES.PADDING * 1.33,
    },
    social: {
        flex: 0,
        flexDirection: 'row',
        justifyContent: 'space-between',
    },
    socialLabel: {
        color: COLORS.WHITE,
        flex: 1,
        fontWeight: '500',
        textAlign: 'center',
    },
    subtitle: {
        color: COLORS.GREY,
        fontSize: SIZES.SUBTITLE,
    },
    title: {
        fontSize: SIZES.TITLE,
        fontWeight: '700',
        letterSpacing: 1,
        marginBottom: SIZES.BASE,
        color: COLORS.ORANGE,
        marginTop: '20%',
        alignSelf: 'center'
    },
});