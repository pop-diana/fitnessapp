import React, { Component } from 'react';
import {
  Text,
  TextInput,
  StyleSheet,
  View,
  SafeAreaView,
  TouchableOpacity,
  ActivityIndicator,
  Button, 
  Alert
} from 'react-native';
import { LinearGradient } from 'expo-linear-gradient';
import RNPickerSelect from 'react-native-picker-select';
import { database } from '../../config/config';

const COLORS = {
  WHITE: '#FFF',
  BLACK: '#000',
  BLUE: '#69B1D6',
  ORANGE: '#F7773B',
  DARK_ORANGE: '#FE5000',
  GREY: '#AFAFAF',
  DARK_GREY: '#90919E',
};

const SIZES = {
  BASE: 6,
  FONT: 14,
  TITLE: 34,
  SUBTITLE: 11,
  LABEL: 12,
  PADDING: 9
};

export default class PlanWorkout extends Component {


  static navigationOptions = {
    headerShown: false
  }

  constructor(props) {
    super(props);
    this.state = {
      loggedin: true,
      loading: false,
      user: props.navigation.state.params.user,
      title: null,
      details: null,
      repeat: null,
      items: [
        {
          label: 'Daily',
          value: 'Daily',
        },
        {
          label: 'Weekly',
          value: 'Weekly',
        },
        {
          label: 'Monthly',
          value: 'Monthly',
        },
      ],
    };
    var that = this;
  }

  saveWorkout() {
    let workout = {
      title: this.state.title,
      details: this.state.details,
      repeat: this.state.repeat
    };
    database.ref(this.state.user.user.uid).child('workouts').push(workout);
    Alert.alert("Workout created!");
  }


  render() {
    const { loggedin, loading, title, details } = this.state;
    const isValid = title && details;
    return (
      <>
        <View >
          <Text style={styles.title}>Plan workout</Text>
          <View style={styles.inputContainer}>
            <TextInput
              value={title}
              style={styles.input}
              placeholder="Workout title"
              placeholderTextColor={COLORS.DARK_GREY}
              onChangeText={value => this.setState({ title: value })}
            />
          </View>
          <View style={styles.selectContainer}>
            <RNPickerSelect
                    placeholder={{
                        label: 'Repeat',
                        value: null,
                    }}
                    items={this.state.items}
                    
                    onValueChange={(value) => {
                        this.setState({
                            repeat: value,
                        });
                    }}
               
                    value={this.state.repeat}

                />
          </View>
          <View style={styles.inputContainer}>
            <TextInput
              value={details}
              style={styles.textInput}
              multiline
              numberOfLines={4}
              placeholder="TO DO"
              placeholderTextColor={COLORS.DARK_GREY}
              onChangeText={value => this.setState({ details: value })}
            />
          </View>

          <TouchableOpacity
            disabled={!isValid}
            style={{ marginTop: SIZES.PADDING * 1.5, paddingLeft: 30, paddingRight: 30 }}
            onPress={() => this.saveWorkout()}>
            <LinearGradient
              style={[styles.button, styles.signin]}
              colors={isValid ? [COLORS.ORANGE, COLORS.DARK_ORANGE] : [COLORS.GREY, COLORS.DARK_GREY]}
              start={{ x: 0, y: 1 }}
              end={{ x: 1, y: 0 }}>
              <Text
                style={{
                  fontWeight: '500',
                  letterSpacing: 0.5,
                  color: COLORS.WHITE,
                  backgroundColor: 'transparent',
                }}>
                Submit
                </Text>
            </LinearGradient>
          </TouchableOpacity>
        </View>

      </>
    )
  }
}

const styles = StyleSheet.create({
  button: {
    alignItems: 'center',
    borderRadius: SIZES.BASE,
    justifyContent: 'center',
    padding: SIZES.PADDING / 0.83,
  },
  container: {
    flex: 1,
    paddingHorizontal: SIZES.PADDING,
    paddingVertical: SIZES.PADDING * 2,
  },
  divider: {
    alignItems: 'center',
    backgroundColor: COLORS.GREY,
    height: StyleSheet.hairlineWidth,
    justifyContent: 'center',
    marginVertical: SIZES.PADDING * 2,
    width: '50%',
  },
  dividerLabel: {
    backgroundColor: COLORS.WHITE,
    color: COLORS.GREY,
    fontSize: SIZES.SUBTITLE,
    paddingHorizontal: SIZES.BASE,
    position: 'absolute',
  },
  input: {
    borderColor: COLORS.GREY,
    borderRadius: SIZES.BASE,
    borderWidth: StyleSheet.hairlineWidth,
    fontSize: SIZES.FONT,
    padding: SIZES.PADDING * 1.5,
    marginTop: '10%',
  },
  textInput: {
    borderColor: COLORS.GREY,
    borderRadius: SIZES.BASE,
    borderWidth: StyleSheet.hairlineWidth,
    fontSize: SIZES.FONT,
    padding: SIZES.PADDING * 1.5,
    paddingVertical: 0

  },
  selectContainer: {
    marginBottom: 10,
    marginTop: 20,
    paddingLeft: 30,
    paddingRight: 15,
  },
  inputContainer: {
    marginBottom: SIZES.PADDING,
    paddingLeft: 30,
    paddingRight: 30
  },
  label: {
    color: COLORS.DARK_GREY,
    fontSize: SIZES.FONT,
    marginBottom: SIZES.BASE,
  },
  signin: {
    paddingVertical: SIZES.PADDING * 1.33,
  },
  social: {
    flex: 0,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  socialLabel: {
    color: COLORS.WHITE,
    flex: 1,
    fontWeight: '500',
    textAlign: 'center',
  },
  subtitle: {
    color: COLORS.GREY,
    fontSize: SIZES.SUBTITLE,
  },
  title: {
    fontSize: SIZES.TITLE,
    fontWeight: '600',
    letterSpacing: 1,
    marginBottom: SIZES.BASE,
    color: COLORS.DARK_ORANGE,
    marginTop: '35%',
    alignSelf: 'center'
  },
});